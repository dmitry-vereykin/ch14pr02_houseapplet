/**
 * Created by Dmitry Vereykin on 7/28/2015.
 */
import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class HouseApplet extends JApplet {
    private boolean leftWindowsClosed = true;
    private boolean rightWindowsClosed = true;
    private boolean doorClosed = true;

    private final int HOUSE_WIDTH = 200;
    private final int HOUSE_HEIGHT = 80;
    private final int HOUSE_X = 20;
    private final int HOUSE_Y = 30;
    private final int ROOF_TOP = 0;
    private final int WINDOW_WIDTH = 50;
    private final int WINDOW_HEIGHT = 30;
    private final int DOOR_WIDTH = 40;
    private final int DOOR_HEIGHT = 70;
    private final int DOOR_Y = 40;
    private final int DOOR_X = (((HOUSE_WIDTH / 2) + HOUSE_X) - (DOOR_WIDTH / 2));
    private final int LEFT_WINDOW_X = (((HOUSE_WIDTH / 4) + HOUSE_X) - (WINDOW_WIDTH / 2));
    private final int LEFT_WINDOW_Y  = 40;
    private final int RIGHT_WINDOW_X  = (((HOUSE_WIDTH / 4) * 3) + HOUSE_X)-(WINDOW_WIDTH / 2);
    private final int RIGHT_WINDOW_Y  = LEFT_WINDOW_Y;
    private final int KNOB_WIDTH = 10;
    private final int KNOB_HEIGHT = KNOB_WIDTH;

    public void init() {
        getContentPane().setBackground(Color.white);
        addMouseListener(new MyMouseListener());
    }

    public void paint(Graphics g) {
        super.paint(g);
        drawHouse(g);

        if(leftWindowsClosed)
            drawClosedWindow(g, LEFT_WINDOW_X, LEFT_WINDOW_Y);
        else
            drawOpenWindow(g, LEFT_WINDOW_X, LEFT_WINDOW_Y);

        if(rightWindowsClosed)
            drawClosedWindow(g, RIGHT_WINDOW_X, RIGHT_WINDOW_Y);
        else
            drawOpenWindow(g, RIGHT_WINDOW_X, RIGHT_WINDOW_Y);

        if(doorClosed)
            drawClosedDoor(g, DOOR_X, DOOR_Y);
        else
            drawOpenDoor(g, DOOR_X, DOOR_Y);
    }

    private void drawHouse(Graphics g) {
        g.setColor(Color.black);
        g.drawRect(HOUSE_X, HOUSE_Y, HOUSE_WIDTH, HOUSE_HEIGHT);

        g.drawLine((HOUSE_X - 10), HOUSE_Y, (HOUSE_WIDTH + 30), HOUSE_Y);
        g.drawLine((HOUSE_X - 10), HOUSE_Y, ((HOUSE_WIDTH / 2) + HOUSE_X), ROOF_TOP);
        g.drawLine(((HOUSE_WIDTH / 2) + HOUSE_X) , ROOF_TOP, (HOUSE_WIDTH + 30), HOUSE_Y);
    }

    private void drawOpenWindow(Graphics g, int x, int y) {
        g.setColor(Color.black);
        g.fillRect(x, y, WINDOW_WIDTH, WINDOW_HEIGHT);
    }

    private void drawClosedWindow(Graphics g, int x, int y) {
        g.setColor(Color.black);
        g.drawRect(x, y, WINDOW_WIDTH, WINDOW_HEIGHT);
        g.drawLine(x, (y + WINDOW_HEIGHT / 2), (x + WINDOW_WIDTH), (y + WINDOW_HEIGHT / 2));
        g.drawLine((x + (WINDOW_WIDTH / 2)), (y + WINDOW_HEIGHT), (x + (WINDOW_WIDTH / 2)), y);
    }

    private void drawOpenDoor(Graphics g, int x, int y) {
        g.setColor(Color.black);
        g.fillRect(x, y, DOOR_WIDTH, DOOR_HEIGHT);
    }

    private void drawClosedDoor(Graphics g, int x, int y) {
        g.setColor(Color.black);
        g.drawRect(x, y, DOOR_WIDTH, DOOR_HEIGHT);
        g.fillOval(((x + DOOR_WIDTH) - KNOB_WIDTH), (y + (DOOR_HEIGHT / 2)), KNOB_WIDTH, KNOB_HEIGHT);
    }

    private class MyMouseListener extends MouseAdapter {
        public void mouseClicked(MouseEvent e) {
            if (isInLeftWindow(e.getX(), e.getY()))
                leftWindowsClosed = !leftWindowsClosed;
            else if (isInRightWindow(e.getX(), e.getY()))
                rightWindowsClosed = !rightWindowsClosed;
            else if (isInDoor(e.getX(), e.getY()))
                doorClosed = !doorClosed;

            repaint();
        }

        private boolean isInLeftWindow(int x, int y) {
            boolean status = false;

            if (x >= LEFT_WINDOW_Y && x < (LEFT_WINDOW_X + WINDOW_WIDTH) &&
                y >= LEFT_WINDOW_X && y < (LEFT_WINDOW_Y + WINDOW_HEIGHT))
                status = true;
            return status;
        }

        private boolean isInRightWindow(int x, int y) {
            boolean status = false;

            if (x >= RIGHT_WINDOW_X && x < (RIGHT_WINDOW_X + WINDOW_WIDTH) &&
                y >= RIGHT_WINDOW_Y && y < (RIGHT_WINDOW_Y + WINDOW_HEIGHT))
                status = true;
            return status;
        }

        private boolean isInDoor(int x, int y) {
            boolean status = false;

            if (x >= DOOR_X && x < (DOOR_X + DOOR_WIDTH) &&
                y >= DOOR_Y && y < (DOOR_Y + DOOR_HEIGHT))
                status = true;
            return status;
        }
    }
}
